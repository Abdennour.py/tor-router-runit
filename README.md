# Tor Router Runit

Tor Router allow you to use TOR as a transparent proxy and send all your trafic under TOR **INCLUDING DNS REQUESTS**
This version of Tor Router can be runed in runit only :)


# Instalation :
**It script require root privileges**
1. Open a terminal and clone the script using the following command:
```
→ git clone https://gitlab.com/Abdennour.py/tor-router-runit.git
→ cd tor-router
→ chmod +x *
→ doas ./install.sh
```
2. Start the tor service :
```
doas sv start tor
doas sv status tor
```
3. Execute the tor-router script as root
```
doas tor-router
doas sv restart tor
```


